from django.urls import resolve
from django.test import TestCase, Client
from django.http import HttpRequest
from .views import home
# Create your tests here.

class UnitTest(TestCase):
	def test_app_url_exist(self):
		response=Client().get('/')
		self.assertEqual(response.status_code,200)

	def test_function_used_correct(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

	def test_home_return_correct_html(self):
		response = self.client.get('/') 
		self.assertTemplateUsed(response, 'home.html')
